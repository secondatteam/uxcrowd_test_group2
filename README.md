Автоматизированные тесты для [UXCrowd](https://preprod.uxcrowd.ru/)
=
## Автоматизированные тесты на основе Test Cases из Test Rail

- [Основная реализация](https://gitlab.com/secondatteam/uxcrowd_test_group2/tree/dev)

- [Реализация удаленного запуска](https://gitlab.com/secondatteam/uxcrowd_test_group2/tree/with_remote)